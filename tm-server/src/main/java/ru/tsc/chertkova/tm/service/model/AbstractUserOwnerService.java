package ru.tsc.chertkova.tm.service.model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tsc.chertkova.tm.api.service.model.IUserOwnerService;
import ru.tsc.chertkova.tm.api.service.model.IUserService;
import ru.tsc.chertkova.tm.model.AbstractUserOwnerModel;

@Service
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractUserOwnerService<M extends AbstractUserOwnerModel>
        extends AbstractService<M> implements IUserOwnerService<M> {

    @Nullable
    @Autowired
    protected IUserService userService;

}
