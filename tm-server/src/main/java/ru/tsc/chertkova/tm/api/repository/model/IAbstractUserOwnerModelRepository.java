package ru.tsc.chertkova.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.model.AbstractFieldsModel;

import java.util.List;

public interface IAbstractUserOwnerModelRepository<M extends AbstractFieldsModel> extends IAbstractRepository<M> {

    void add(@NotNull String userId, @NotNull M model);

    void clear(@NotNull String userId);

    @NotNull
    List<M> findAll(@NotNull String userId);

    @Nullable
    M findById(@NotNull String userId, @NotNull String id);

    int getSize(@NotNull String userId);

    M removeById(@NotNull String userId, @NotNull String id);

    void update(@NotNull String userId, @NotNull M model);

}
