package ru.tsc.chertkova.tm.configuration;

import org.hibernate.cfg.Environment;
import org.hibernate.event.service.spi.EventListenerRegistry;
import org.hibernate.event.spi.EventType;
import org.hibernate.internal.SessionFactoryImpl;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import ru.tsc.chertkova.tm.api.service.IPropertyService;
import ru.tsc.chertkova.tm.listener.EntityListener;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
@ComponentScan("ru.tsc.chertkova.tm")
public class ContextConfiguration {

    @Bean
    public DataSource dataSource(@NotNull final IPropertyService propertyService) {
        @NotNull final DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(propertyService.getDatabaseDriver());
        dataSource.setUrl(propertyService.getDatabaseUrl());
        dataSource.setUsername(propertyService.getDatabaseUsername());
        dataSource.setPassword(propertyService.getDatabasePassword());
        return dataSource;
    }

    @Bean
    public PlatformTransactionManager transactionManager(
            @NotNull final LocalContainerEntityManagerFactoryBean entityManagerFactory,
            @NotNull final EntityListener entityListener
    ) {
        @NotNull final JpaTransactionManager transactionManager = new JpaTransactionManager();
        @Nullable final EntityManagerFactory factory = entityManagerFactory.getObject();
        initListeners(factory, entityListener);
        transactionManager.setEntityManagerFactory(factory);
        return transactionManager;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(
            @NotNull final IPropertyService propertyService,
            @NotNull final DataSource dataSource
    ) {
        @NotNull final LocalContainerEntityManagerFactoryBean factoryBean;
        factoryBean = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setDataSource(dataSource);
        factoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        factoryBean.setPackagesToScan("ru.tsc.chertkova.tm.model", "ru.tsc.chertkova.tm.dto.model");
        @NotNull final Properties properties = new Properties();
        properties.put(Environment.DRIVER, propertyService.getDatabaseDriver());
        properties.put(Environment.URL, propertyService.getDatabaseUrl());
        properties.put(Environment.USER, propertyService.getDatabaseUsername());
        properties.put(Environment.PASS, propertyService.getDatabasePassword());
        properties.put(Environment.DIALECT, propertyService.getDatabaseDialect());
        properties.put(Environment.HBM2DDL_AUTO, propertyService.getDatabaseHBM2DDL());
        properties.put(Environment.SHOW_SQL, propertyService.getDatabaseShowSQL());
        properties.put(Environment.USE_SECOND_LEVEL_CACHE, propertyService.getDatabaseUseSecondLvlCache());
        properties.put(Environment.USE_QUERY_CACHE, propertyService.getDatabaseUseQueryCache());
        properties.put(Environment.USE_MINIMAL_PUTS, propertyService.getDatabaseUseMinimalPuts());
        properties.put(Environment.CACHE_REGION_PREFIX, propertyService.getDatabaseCacheRegionPrefix());
        properties.put(Environment.CACHE_PROVIDER_CONFIG, propertyService.getDatabaseCacheConfigFile());
        properties.put(Environment.CACHE_REGION_FACTORY, propertyService.getDatabaseCacheFactoryClass());
        factoryBean.setJpaProperties(properties);
        return factoryBean;
    }

    private void initListeners(@NotNull final EntityManagerFactory factory,
                               @NotNull final EntityListener entityListener) {
        @NotNull final SessionFactoryImpl sessionFactory = factory.unwrap(SessionFactoryImpl.class);
        @NotNull final EventListenerRegistry registry = sessionFactory
                .getServiceRegistry().getService(EventListenerRegistry.class);
        registry.getEventListenerGroup(EventType.POST_INSERT).appendListener(entityListener);
        registry.getEventListenerGroup(EventType.POST_DELETE).appendListener(entityListener);
        registry.getEventListenerGroup(EventType.POST_UPDATE).appendListener(entityListener);
        registry.getEventListenerGroup(EventType.PRE_INSERT).appendListener(entityListener);
        registry.getEventListenerGroup(EventType.PRE_DELETE).appendListener(entityListener);
        registry.getEventListenerGroup(EventType.PRE_UPDATE).appendListener(entityListener);
        registry.getEventListenerGroup(EventType.POST_LOAD).appendListener(entityListener);
    }

}
