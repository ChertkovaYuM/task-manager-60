package ru.tsc.chertkova.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.dto.model.TaskDTO;

import java.util.List;

public interface ITaskDtoRepository extends IAbstractUserOwnerModelDtoRepository<TaskDTO> {

    void add(@NotNull String userId,
             @NotNull TaskDTO model);

    void clear(@NotNull String userId);

    @NotNull
    List<TaskDTO> findAll(@NotNull String userId);

    @Nullable
    TaskDTO findById(@NotNull String userId,
                     @NotNull String id);

    int getSize(@NotNull String userId);

    void removeById(@NotNull String userId,
                    @NotNull String id);

    void update(@NotNull String userId,
                @NotNull TaskDTO model);

    @Nullable
    List<TaskDTO> findAllByProjectId(
            @Nullable String userId,
            @Nullable String projectId);

    void changeStatus(@NotNull String id,
                      @NotNull String userId,
                      @NotNull String status);

    int existsById(@Nullable String id);

    void bindTaskToProject(@NotNull String taskId,
                           @NotNull String projectId,
                           @NotNull String userId);

}
