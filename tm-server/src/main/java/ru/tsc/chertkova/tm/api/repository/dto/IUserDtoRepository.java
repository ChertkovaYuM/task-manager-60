package ru.tsc.chertkova.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.dto.model.UserDTO;
import ru.tsc.chertkova.tm.enumerated.Role;

import java.util.List;

public interface IUserDtoRepository extends IAbstractDtoRepository<UserDTO> {

    void add(@NotNull UserDTO model);

    void clear();

    @NotNull
    List<UserDTO> findAll();

    @Nullable
    UserDTO findById(@NotNull String id);

    int getSize();

    void removeById(@NotNull String id);

    void update(UserDTO model);

    @Nullable
    UserDTO findByEmail(@NotNull String email);

    @Nullable
    UserDTO findByLogin(@NotNull String login);

    void removeByLogin(@NotNull String login);


    int isLoginExist(@NotNull String login);

    int isEmailExist(@NotNull String email);

    void changeRole(@NotNull String id,
                    @NotNull Role role);

    int existsById(@NotNull String id);

    void setPassword(@NotNull String id,
                     @NotNull String passwordHash);

    void setLockedFlag(@NotNull String login,
                       @NotNull Boolean locked);

}
