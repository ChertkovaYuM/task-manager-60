package ru.tsc.chertkova.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.chertkova.tm.dto.request.user.UserUpdateProfileRequest;
import ru.tsc.chertkova.tm.enumerated.Role;
import ru.tsc.chertkova.tm.event.ConsoleEvent;
import ru.tsc.chertkova.tm.listener.AbstractUserListener;
import ru.tsc.chertkova.tm.util.TerminalUtil;

@Component
public final class UserUpdateProfileListener extends AbstractUserListener {

    @NotNull
    public static final String NAME = "user-update-profile";

    @NotNull
    public static final String DESCRIPTION = "Update user profile.";

    @NotNull
    @Override
    public String command() {
        return NAME;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@userUpdateProfileListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[USER UPDATE PROFILE]");
        System.out.println("ENTER FIRST NAME:");
        @Nullable final String firstName = TerminalUtil.nextLine();
        System.out.println("ENTER LAST NAME:");
        @Nullable final String lastName = TerminalUtil.nextLine();
        System.out.println("ENTER MIDDLE NAME:");
        @Nullable final String middleName = TerminalUtil.nextLine();
        getUserEndpoint().updateUserProfile(new UserUpdateProfileRequest(getToken(),
                firstName, lastName, middleName));
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
