package ru.tsc.chertkova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.chertkova.tm.api.endpoint.IAuthEndpoint;
import ru.tsc.chertkova.tm.api.endpoint.IUserEndpoint;
import ru.tsc.chertkova.tm.api.service.IPropertyService;
import ru.tsc.chertkova.tm.dto.request.user.*;
import ru.tsc.chertkova.tm.dto.response.user.UserLoginResponse;
import ru.tsc.chertkova.tm.dto.response.user.UserProfileResponse;
import ru.tsc.chertkova.tm.dto.response.user.UserRegistryResponse;
import ru.tsc.chertkova.tm.dto.response.user.UserUpdateProfileResponse;
import ru.tsc.chertkova.tm.marker.IntegrationCategory;
import ru.tsc.chertkova.tm.model.User;
import ru.tsc.chertkova.tm.service.PropertyService;

@Category(IntegrationCategory.class)
public class UserEndpointTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final String host = propertyService.getHost();

    @NotNull
    private final String port = propertyService.getPort();

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(host, port);

    @NotNull
    private final IUserEndpoint userEndpoint = IUserEndpoint.newInstance(host, port);

    @Nullable
    private String adminToken;

    @Nullable
    private User userBefore;

    @Before
    public void init() {
        @NotNull final UserLoginResponse loginResponse = authEndpoint.login(
                new UserLoginRequest("admin", "admin"));
        adminToken = loginResponse.getToken();
    }

    @Test
    public void profile() {
        @NotNull final UserLoginResponse response = authEndpoint.login(new UserLoginRequest("admin", "admin"));
        @Nullable String token = response.getToken();
        Assert.assertThrows(Exception.class, () -> userEndpoint.profile(new UserProfileRequest()));
        Assert.assertThrows(Exception.class, () -> userEndpoint.profile(new UserProfileRequest("token")));
        UserProfileResponse responseProfile = userEndpoint.profile(new UserProfileRequest(token));
        Assert.assertNotNull(responseProfile);
        @Nullable User user = responseProfile.getUser();
        Assert.assertNotNull(user);
        Assert.assertEquals("admin", user.getLogin());
    }

    @Test
    public void registryUser() {
        Assert.assertThrows(Exception.class, () ->
                userEndpoint.registryUser(new UserRegistryRequest(null, null, null)));
        Assert.assertThrows(Exception.class, () ->
                userEndpoint.registryUser(new UserRegistryRequest("login", "password", "")));
        userEndpoint.registryUser(new UserRegistryRequest( "login", "password", "login@login.ru"));
        @NotNull final UserLoginResponse loginResponse = authEndpoint.login(
                new UserLoginRequest("login", "password"));
        Assert.assertNotNull(loginResponse.getToken());
        userEndpoint.removeUser(new UserRemoveRequest(adminToken, "login"));
    }

    @Test
    public void removeUser() {
        UserRegistryResponse registryResponse = userEndpoint.registryUser(
                new UserRegistryRequest("test1", "test1", "test1@test.ru"));
        userBefore = registryResponse.getUser();
        Assert.assertThrows(Exception.class, () ->
                userEndpoint.removeUser(new UserRemoveRequest(null, userBefore.getLogin())));
        Assert.assertThrows(Exception.class, () ->
                userEndpoint.removeUser(new UserRemoveRequest(adminToken, userBefore.getLogin())));
        Assert.assertThrows(Exception.class, () ->
                authEndpoint.login(new UserLoginRequest("test1", "test1")));
    }

    @Test
    public void updateUser() {
        UserRegistryResponse registryResponse = userEndpoint.registryUser(
                new UserRegistryRequest("test", "test", ""));
        userBefore = registryResponse.getUser();
        @NotNull final UserLoginResponse loginResponse = authEndpoint.login(new UserLoginRequest("test", "test"));
        @Nullable String token = loginResponse.getToken();
        @NotNull final String firstName = "first";
        @NotNull final String lastName = "last";
        @NotNull final String middleName = "middle";
        Assert.assertThrows(Exception.class, () ->
                userEndpoint.updateUserProfile(
                        new UserUpdateProfileRequest(null, firstName, lastName, middleName)));
        @NotNull UserUpdateProfileResponse response =
                userEndpoint.updateUserProfile(new UserUpdateProfileRequest(token, firstName, lastName, middleName));
        @Nullable User user = response.getUser();
        Assert.assertNotNull(user);
        Assert.assertEquals(firstName, user.getFirstName());
        Assert.assertEquals(lastName, user.getLastName());
        Assert.assertEquals(middleName, user.getMiddleName());
        userEndpoint.removeUser(new UserRemoveRequest(adminToken, userBefore.getLogin()));
    }

    @Test
    public void showProfileUser() {
        UserRegistryResponse registryResponse = userEndpoint.registryUser(
                new UserRegistryRequest("test", "test", ""));
        userBefore = registryResponse.getUser();
        @NotNull final UserLoginResponse loginResponse = authEndpoint.login(new UserLoginRequest("test", "test"));
        @Nullable String token = loginResponse.getToken();
        userEndpoint.profile(new UserProfileRequest(null));
        @NotNull UserProfileResponse response = userEndpoint.profile(new UserProfileRequest(token));
        @Nullable User user = response.getUser();
        Assert.assertNotNull(user);
        Assert.assertEquals("test", user.getLogin());
        userEndpoint.removeUser(new UserRemoveRequest(adminToken, userBefore.getLogin()));
    }

    @Test
    public void changePassword() {
        UserRegistryResponse registryResponse = userEndpoint.registryUser(
                new UserRegistryRequest("user", "user", "user@user.ru"));
        userBefore = registryResponse.getUser();
        @NotNull String oldPassword = "user";
        @NotNull String newPassword = "password";
        @NotNull final UserLoginResponse response = authEndpoint.login(new UserLoginRequest("user", oldPassword));
        @Nullable String token = response.getToken();
        Assert.assertNotNull(token);
        Assert.assertThrows(Exception.class, () ->
                userEndpoint.changeUserPassword(new UserChangePasswordRequest(token, newPassword)));
        Assert.assertThrows(Exception.class, () ->
                authEndpoint.login(new UserLoginRequest("user", oldPassword)));
        @NotNull final UserLoginResponse responseAfterChange = authEndpoint.login(
                new UserLoginRequest("user", newPassword));
        Assert.assertNotNull(responseAfterChange.getToken());
        userEndpoint.removeUser(new UserRemoveRequest(adminToken, userBefore.getLogin()));
    }

    @Test
    public void lockUser() {
        UserRegistryResponse registryResponse = userEndpoint.registryUser(
                new UserRegistryRequest("user", "user", "user@user.com"));
        userBefore = registryResponse.getUser();
        Assert.assertThrows(Exception.class, () ->
                userEndpoint.lockUser(new UserLockRequest(null, userBefore.getLogin())));
        Assert.assertThrows(Exception.class, () ->
                userEndpoint.lockUser(new UserLockRequest(adminToken, userBefore.getLogin())));
        Assert.assertThrows(Exception.class, () ->
                authEndpoint.login(new UserLoginRequest("user", "user")));
        Assert.assertThrows(Exception.class, () ->
                userEndpoint.removeUser(new UserRemoveRequest(adminToken, userBefore.getLogin())));
    }

    @Test
    public void unlockUser() {
        UserRegistryResponse registryResponse = userEndpoint.registryUser(
                new UserRegistryRequest("user", "user", "user@user.ru"));
        userBefore = registryResponse.getUser();
        Assert.assertThrows(Exception.class, () ->
                userEndpoint.lockUser(new UserLockRequest(adminToken, userBefore.getLogin())));
        Assert.assertThrows(Exception.class, () ->
                authEndpoint.login(new UserLoginRequest("user", "user")));
        Assert.assertThrows(Exception.class, () ->
                userEndpoint.unlockUser(new UserUnlockRequest(null, userBefore.getLogin())));
        Assert.assertThrows(Exception.class, () ->
                userEndpoint.unlockUser(new UserUnlockRequest(adminToken, userBefore.getLogin())));
        @NotNull final UserLoginResponse response = authEndpoint.login(new UserLoginRequest("user", "user"));
        @Nullable String token = response.getToken();
        Assert.assertNotNull(token);
        Assert.assertThrows(Exception.class, () ->
                userEndpoint.removeUser(new UserRemoveRequest(adminToken, userBefore.getLogin())));
    }

}
