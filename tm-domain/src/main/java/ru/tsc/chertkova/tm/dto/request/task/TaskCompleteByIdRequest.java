package ru.tsc.chertkova.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.dto.request.user.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public class TaskCompleteByIdRequest extends AbstractUserRequest {

    @Nullable
    private String id;

    public TaskCompleteByIdRequest(@Nullable String token, @Nullable String id) {
        super(token);
        this.id = id;
    }

}
