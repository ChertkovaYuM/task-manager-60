package ru.tsc.chertkova.tm.configuration;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import ru.tsc.chertkova.tm.listener.LoggerListener;

import javax.jms.ConnectionFactory;
import javax.jms.MessageListener;

@ComponentScan("ru.tsc.chertkova.tm")
public final class LoggerConfiguration {

    @Bean
    public ConnectionFactory factory() {
        @NotNull final ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory(ActiveMQConnection.DEFAULT_BROKER_URL);
        factory.setTrustAllPackages(true);
        return factory;
    }

    @Bean
    public MessageListener listener() {
        return new LoggerListener();
    }

}
